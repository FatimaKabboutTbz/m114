# Aufgaben zu Kryptografie

## Symmetrische Verschlüssungsverfahren

### 1.)Cryptool-Applikation installieren
wurde instaliiert
![cryptool](./Images/cryptool.png)

### 2.) Rotationschiffre
Verschlüsselte Zitate: Mit einer Rotation von 3 (ROT-3) können folgende Zitate entschlüsselt werden:
- DER ANGRIFF ERFOLGT ZUR TEEZEIT DIE WUERFEL SIND GEFALLEN ICH KAM SAH UND SIEGTE TEILE UND HERRSCHE

### 3.) Vigenèreverschlüsselung
- Einfaches Beispiel: Verschlüsseln Sie das Wort "BEEF" mit dem Schlüsselwort "AFFE" zu "BJJJ".
- Entschlüsselung: Entschlüsseln Sie den Geheimtext "WRKXQT" mit dem Schlüsselwort "SECRET" zu "ENIGMA".
- Komplexe Analyse: Verwenden Sie das Vigenère-Analysetool in Cryptool1, um komplexere Chiffren zu analysieren und zu entschlüsseln.

### 4.)Vigenèrecodeanalyse
Verschlüsselte Botschaft: USP JHYRH ZZB GTV CJ WQK OCLGQVFQK GAYKGVFGX NS ISBVB MYBC MWCC NS JOEVB GTV KRQFV AGK XCUSP VFLVBLLBE ESSEILUBCLBXZU SENSWFGVRCES SER CZBCE ILUOLBPYISL CCSZG VZJ

### 5.) XOR-Stromchiffre
- Anleitung: Verschlüsseln Sie die Dezimalzahl 4711 mithilfe einer XOR-Stromchiffre und dem binären Schlüssel 1000'1101. Die binäre Darstellung von 4711 ist «0001'0010'0110'0111» und durch XOR mit dem Schlüssel ergibt sich die Chiffre «1001'1111'1110'1010».

### 6.) AES (Advanced Encryption Standard)
- Studium: Betrachten Sie die AES-Rijndael-Animation auf Cryptool Online, um ein tieferes Verständnis des AES (Advanced Encryption Standard) zu erhalten. AES-Animation ansehen

### 7.) Wie sicher ist mein Passwort?
- Tool: Nutzen Sie den Passwort-Qualitätsmesser in Cryptool, um die Sicherheit Ihres Lieblingspassworts zu überprüfen.

## Asymmetrische Verschlüsselungsverfahren
### 1.)Experimentieren mit Cryptool

### 2.) RSA-Verschlüsselung
Es konnte nicht richtig entschlüsselt werden.

### 3.)
Im Gegensatz zum Diffie-Hellman-Verfahren (für Schlüsseltausch) kann RSA einen kompletten Text verschlüsseln.

### 4.)
Moderne Verschlüsselungsverfahren arbeiten hybrid

### 6.) Hashwert
Im Cryptool erledigt

### 7.) Dokument signieren
Im Cryptool erledigt

### 8.) Hashwert-Manipulation bei der digitalen Signatur
Im Cryptool erledigt
## Die Schlüsselverwaltung
### 1.) Wie kann ich einen Public-Key verifizieren?
- Mit dem zugehörigen Zertifikat und der Unterschrift einer vertrauenswürdigen Stelle (CA).
### 2.) Was versteht man unter Public Key Infrastruktur (PKI)?
- Ein System zur Verwaltung von Schlüsseln und Zertifikaten, das sichere Kommunikation ermöglicht.
### 3.) Was bedeutet Certification-Authority (CA) und was Trust-Center (TC)?
- CA (Certification Authority): Stelle, die digitale Zertifikate ausstellt und verifiziert.
- Trust-Center (TC): Synonym für CA, oft in Deutschland verwendet.

## Sicheres Internet und Zertifikat

### 1.) Wer hat das Zertifikat für die Bankwebseite www.ubs.com ausgestellt und wie lange ist es gültig?
- Das Zertifikat wurde von DigiCert ausgestellt. Die Gültigkeitsdauer variiert, oft ein bis zwei Jahre.

### 2.) Wer hat das Zertifikat für die für die Schulwebseite www.tbz.ch ausgestellt und wie lange ist es gültig?
- Das Zertifikat wurde von SwissSign ausgestellt. Die Gültigkeitsdauer variiert, oft ein bis zwei Jahre.

### 3.) Wer hat das Zertifikat für die für die Webseite www.example.ch ausgestellt und wie lange ist es gültig? 
- Das Zertifikat wurde von Let's Encrypt ausgestellt. Die Gültigkeitsdauer beträgt 90 Tage.

### 4.) Wählen sie irgendeine Applikation aus, die auf ihrem PC installiert ist. Stellen sie sich nun vor, sie müssten diese von Hand aktualisieren oder aus Kompatibilitätsgründen auf eine frühere Version zurückstufen. Wo finden sie aktuelle und frühere Versionen ihrer Software und wie wird sichergestellt, dass die dort angebotene SW-Version auch wirklich echt ist bzw. vom SW-Entwickler stammt?
- Aktuelle und frühere Versionen findet man auf der offiziellen Webseite des Entwicklers oder auf vertrauenswürdigen Plattformen wie GitHub. Echtheit wird durch digitale Signaturen oder Prüfsummen (Hashes) sichergestellt.

### 5.) Erstellen sie eine virtuelle Linux-Maschine mit z.B. VirtualBox und Ubuntu. Richten sie nun auf ihrem WIN-PC eine Remoteverbindung via ssh zu ihrem Linux-PC ein. Überprüfen sie die Verbindung. Wäre auch eine graphische Anbindung möglich?
- Ja, mit Tools wie VNC oder X11-Forwarding über SSH.

### 6.) In dieser Übung untersuchen wir eine http-Verbindung und eine https-Verbindung mit dem Network-Sniffer Wireshark: https://www.wireshark.org/ http://www.example.ch https://www.zkb.ch Untersuchen sie speziell die OSI-Layer 2,3,4 und 7. Was stellen sie fest? Wo liegen die Unterschiede zwischen http und https? Zusatzfrage: Kann man mit Wireshark bei einer https-Verbindung trotzdem herausfinden, welche Webseite besucht wurde?
- Unterschiede: HTTPS verschlüsselt die Daten, HTTP nicht.
- Mit Wireshark kann man die Ziel-IP-Adresse bei HTTPS sehen, aber den Inhalt nicht.

### 7.) Öffnen sie die beiden folgenden Webseiten und achten sie auf die Unterschiede in der Webadresszeile. Was stellen sie bezüglich Protokoll und Zertifikat fest? https://juergarnold.ch https://www.zkb.ch
- Beide verwenden HTTPS und haben Zertifikate, die in der Adresszeile durch ein Schloss-Symbol angezeigt werden. Unterschiede können in den Zertifikatsdetails und Ausstellern liegen.

### 8.) Wenn sie sich mit Zertifikaten befassen, fallen ihnen früher oder später folgende Anbieter bzw. Webseiten auf: http://www.cacert.org https://letsencrypt.org/de Was genau wird hier zu welchen Konditionen angeboten?
- CAcert: Kostenlose Zertifikate nach Identitätsüberprüfung.
- Let's Encrypt: Kostenlose, automatisierte Zertifikate.

### 9.) Folgende TLS Zertifikatsarten werden unterschieden: Domain Validated, Organization Validated und Extended Validation. Sie möchten einen Webshop betreiben, wo mit Kreditkarte bezahlt werden kann. Welcher Zertifikatstyp ist der richtige?
- Extended Validation (EV) Zertifikat.

### 10.) Studieren sie den Beitrag auf der Webseite Let's Encrypt "Wie es funktioniert" https://letsencrypt.org/de/how-it-works/ Was ist der Unterschied zwischen OpenPGP und X.509?
- OpenPGP: Hauptsächlich für E-Mail-Verschlüsselung, nutzerbasiert.
- X.509: Für TLS/SSL-Zertifikate, von CAs ausgestellt.

### 11.) Erklären sie den Aufruf einer sicheren Webseite. (HTTPS) Wie ist der Ablauf beim Protokoll TLS? Wo genau kommen die Zertifikate ins Spiel?
- Client fordert Webseite an.
- Server sendet Zertifikat.
- Client prüft Zertifikat und erstellt einen Sitzungsschlüssel.
- Daten werden verschlüsselt übertragen.

### 12.) Was bedeutet S/MIME?
- Secure/Multipurpose Internet Mail Extensions. Standard für E-Mail-Verschlüsselung und -Signatur.

### 13.) Aus gesetzlichen Gründen sind sie verpflichtet, den gesamten geschäftlichen EMail-Verkehr zu archivieren, auch den verschlüsselten. Was ist das Problem dabei und wie könnte man dies lösen?
- Problem: E-Mails sind ohne private Schlüssel unlesbar. 
- Lösung: Schlüsselmanagement und rechtzeitige Entschlüsselung vor Archivierung.

## PGP und OpenPGP
### 1.) GPG4WIN auf dem eigenen Notebook installieren
- erledigt
### 2.) Mit GPG4WIN/Kleopatra ein Schlüsselpaar erstellen
- erledigt
### 20.) Fremden Public-Key verifizieren
- erledigt
### 21.) Frage zum OpenPGP-Schlüssel
- erledigt
### 22.) X.509-Schlüsselpaar
- erledigt
### 23.) Mit Gpg4win/Kleopatra eine Nachricht verschlüsseln
- erledigt
### 24.) Mit Gpg4win/Kleopatra eine Nachricht signieren
- erledigt
### 25.) Mit Gpg4win/Kleopatra eine Nachricht verschlüsseln und signieren
- erledigt
### 26.) Vorarbeiten zu E-Mails im Mailclient Thunderbird verschlüsseln
- erledigt
### 27.) Thunderbird auf ihrem PC/Notebook installieren
- erledigt
### 28.) Den Mailclient Thunderbird einrichten
- erledigt
### 29.) Schlüssel in Thunderbird einrichten
- erledigt
### 30.) E-Mail Zielperson aussuchen
- erledigt
### 31.) EMails in Thunderbird verschlüsseln und/oder Signieren
- erledigt
