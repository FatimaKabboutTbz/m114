# Aufgaben zu Bilder Codieren und Komprimieren

## Codieren

### 1.) Bestimmen sie die Farben für die folgenden RGB-Farbcodes (in DEZ und HEX)

- RGB(255, 255, 255) entspricht Farbe: #ffffff (weiss)
- RGB(0,0,0) entspricht Farbe: #000000 (schwarz)
- RGB(252,178,91) entspricht Farbe: #fcb25b (orange)
- #FF0000 entspricht Farbe: rgb(255, 0, 0) (rot)
- #00FF00 entspricht Farbe: rgb(0, 255, 0) (grün)
- #0000FF entspricht Farbe: rgb(0, 0, 255) (blau)
- #FFFF00 entspricht Farbe: rgb(255, 255, 0) (gelb)
- #00FFFF entspricht Farbe: rgb(0, 255, 255) (cyan)
- #FF00FF entspricht Farbe: rgb(255, 0, 255) (magenta)
- #000000 entspricht Farbe: rgb(0, 0, 0) (schwarz)
- #FFFFFF entspricht Farbe: rgb(255, 255, 255) (weiss)
- #00BC00 entspricht Farbe: rgb(0, 188, 0) (grün)

### 2.) Bestimmen sie die Farben für die folgenden prozentualen CMYK-Angaben

- C:0%, M:100%, Y:100%, K:0% entspricht Farbe: rgb(255, 0, 0), #ff0000 (rot)
- C:100%, M:0%, Y:100%, K:0% entspricht Farbe: rgb(0, 255, 0), #00ff00 (grün)
- C:100%, M:100%, Y:0%, K:0% entspricht Farbe: rgb(0, 0, 255), #0000ff (blau)
- C:0%, M:0%, Y:100%, K:0% entspricht Farbe: rgb(255, 255, 0), #ffff00 (gelb)
- C:100%, M:0%, Y:0%, K:0% entspricht Farbe: rgb(0, 255, 255), #00ffff (cyan)
- C:0%, M:100%, Y:0%, K:0% entspricht Farbe: rgb(255, 0, 255), #ff00ff (magenta)
- C:100%, M:100%, Y:100%, K:0% entspricht Farbe: rgb(0, 0, 0), #000000 (schwarz)
- C:0%, M:0%, Y:0%, K:100% entspricht Farbe: rgb(0, 0, 0), #000000 (schwarz)
- C:0%, M:0%, Y:0%, K:0% entspricht Farbe: rgb(255, 255, 255), #ffffff (weiss)
- C:0%, M:46%, Y:38%, K:22% entspricht Farbe: rgb(199, 107, 123), #c76b7b

### 3.) Berechnen sie den theoretischen Speicherbedarf in Bit und in Byte eines unkomprimierten RGB-Bildes mit der Grösse 640 x 480 und 8Bit Auflösung pro Farbkanal.

![3_rechnung](./Images/3_rechnung.png)

### 4.) Sie müssen die Webseite der Firma Muster-GmbH gestalten. Als Hintergrundbild (background-image) soll eine gekachelte Textur verwendet werden. (background-repeat: repeat). Auf diesen Hintergrund wird das Firmenlogo gelegt. Für welche Bildformate werden sie sich entscheiden? Begründen sie!

PNG für die Textur: Transparenz, verlustfreie Qualität. SVG für das Logo: Skalierbarkeit, geringere Dateigröße, beste Qualität auf allen Geräten.

### 5.) Sie haben ein 30-Zoll-Display (Diagonale) im Format 16:10 und 100ppi erworben. Was ist die Pixelauflösung horizontal und vertikal?

2544 x 1590 Pixel

### 6.) Sie drucken ein quadratisches Foto mit einer Kantenlänge von 2000 Pixel mit 600dpi. Wie gross in cm wird dieses?

8.47 cm x 8.47 cm

### 7.) Berechnen sie den Speicherbedarf für ein unkomprimiertes Einzelbild im HD1080p50-Format bei einer True-Color-Farbauflösung.

5.93 MB

### 8.) Welchen Speicherbedarf aus einer HD (Massvorsatz im IEC-Format) hat das Video aus der vorangegangenen Aufgabe bei einer Spieldauer von 3 Minuten?

52.13 GB

### 9.) Ihre Digitalkamera bietet für die Speicherung ihrer Bilder die beiden Formate RAW und JPG an. Wo liegen die Unterschiede und was sind die Verwendungszwecke?

- RAW: Bietet höchste Qualität und Flexibilität bei der Nachbearbeitung, wird jedoch nicht sofort verwendet und benötigt mehr Speicherplatz.
- JPG: Bietet praktische Dateigrößen und sofortige Verwendbarkeit auf Kosten der Bildqualität und Nachbearbeitungsmöglichkeiten.
- Die Wahl zwischen RAW und JPG hängt von den spezifischen Anforderungen und dem Verwendungszweck der Bilder ab.

### 10.) Sie möchten ihr neulich erstelltes Gameplay-Video auf Youtube veröffentlichen. Was sind die technischen Vorgaben dazu? (Format, Bildrate, Farbauflösung, Video-, Audiocodec etc.). Gibt es allenfalls rechtliche Einschränkungen?

Verwenden Sie MP4 (H.264), 1080p oder höher, 24-60 fps, AAC-LC Audio, 128 kbps oder höher. Achten Sie auf Urheberrechte und YouTube-Richtlinien. Überprüfen Sie Spielentwickler-Richtlinien für Gameplay-Aufnahmen.

### 11.) Was ist der Unterschied zwischen dem Interlaced Mode und dem Progressive Mode?

Interlaced Mode zeigt abwechselnd ungerade und gerade Zeilen, während Progressive Mode alle Zeilen nacheinander anzeigt, was zu besserer Bildqualität führt.

### 12.) Was versteht man unter Artefakten und welche kennen sie?

Artefakte sind unerwünschte visuelle Verzerrungen oder Störungen in Bildern oder Videos, die durch Komprimierung, Übertragung oder Bearbeitung entstehen. Beispiele sind Blockbildung, Unschärfe und Farbverzerrungen.

### 13.) Berechnen Sie die Datenrate in GigaBit per Second oder kurz Gbps für die Übertragung eines unkomprimierten digitalen Videosignals HD1080i50 ohne Unterabtastung und 8 Bit Auflösung pro Farbkanal.

Die Datenrate für die Übertragung eines unkomprimierten digitalen Videosignals von HD1080i50 beträgt etwa 2.49 Gbps.

### 14.) Nach wie vielen Minuten unkomprimierten HD1080i50 Video wäre eine DVD-5 (Single-Layer DVD mit 4.7GB) voll?

Auf eine DVD-5 (Single-Layer DVD mit 4.7 GB) passt etwa 1.89 Minuten unkomprimiertes HD1080i50-Video.

### 15.) Was ist der Unterschied zwischen einem Codec und einem Mediencontainer?

Ein Codec komprimiert/dekomprimiert Mediendaten. Ein Mediencontainer organisiert und speichert diese Daten sowie Metadaten in einer Datei.

### 16.) Der folgende Film befasst sich mit der Digitalisierung von analogen Signalen: https://www.youtube.com/watch?v=IZUcqFCsKnA Schauen sie sich diesen Film an und beantworten sie anschliessend die folgenden Fragen
#### a) Warum benötigt man AD-Wandler?
Weil die ALU im Mikroprozessor nur "digital" versteht.
#### b) Warum geht eine A/D-Wandlung immer mit einem Datenverlust einher?
Je nach Samplingrate und Auflösung verliert man Informationen zwischen den Samples/Messwerten. Würde man das digitalisierte Signal wieder in ein analoges Signal zurückwandeln, erhält man einen treppenartigen Verlauf. Die Werte zwischen den Stufen bleiben somit unbekannt.
#### c) Gibt eine höhere oder eine tiefer Samplingrate eine präzisere Abbildung des Originals? Begründen sie!
Je höher die Samplingrate, deso näher kommt die digitale Abbildung dem Original. Bei Musik wird z.B. wegen des menschlichen Hörbereichs von 20Hz bis 20kHz die Samplingrate auf 44kHz festgelegt. Das fordert das sogenannte Abtast-Theorem, das besagt, dass die Samplingfrequenz der doppelten oberen Grenzfrequenz entsprechen muss.
## Komprimieren

### 1.) Um ein gewisses Verständnis für die Luminanz-Chrominanz-Beschreibung von Farben zu erhalten, lösen sie die folgenden Aufgaben.
Online-Tool: https://colorizer.org/
- RGB 255/255/255 entspricht Weiss und ergibt in YCbCr: 1-0-0
- RGB 0/0/0 entspricht Schwarz und ergibt in YCbCr: 0-0-0
- Y:0, Cb:0.5, Cr:0 entspricht der Farbe: Rot
- Y:0, Cb:-0.5, Cr:0 entspricht der Farbe: Grün
- Y:0, Cb:0, Cr:0.5 entspricht der Farbe: Blau
- Y:0, Cb:0, Cr:-0.5 entspricht der Farbe: Grün 
- Y:0.3, Cb:0.5, Cr:-0.17 entspricht der Farbe: Rot

### 2.) Ein RGB-Farbbild benutzt nur die Farbe Weiss als Hintergrund und ein Hellblau mit folgenden Werten: R=33, G=121, B=239 (8 Bit pro Farbkanal). Das Bild soll in ein Graustufenbild umgewandelt werden. Berechnen sie den für das Hellblau entsprechende Grauwert. (8 Bit pro Farbkanal)
Rot: 33 x 0.3 = 9.9 / Grün: 121 x 0.6 = 72.6 / Blau: 239 x 0.1 = 23.9 Summe=106.4
Somit Graustufenwert=106
### 3.) Berechnen sie, wieviel Speicher eingespart wird, wenn ein Bild mit Subsampling 4:1:1 komprimiert wird.
Subsampling: 4:1:1 bedeutet: 100% + 25% + 25% = 150% gegenüber dem Original von 3x 100%. Somit ½ Speicherbedarf
### 4.)Der folgende Youtube-Film beschäftigt sich mit RGB und YCrCb: https://www.youtube.com/watch?v=3dET-EoIMM8 Schauen sie den an und beantworten sie anschliessend diese Fragen:
#### a) Kann man durch die Bildumwandlung vom RGB- in den YCbCr-Farbraum Speicherplatz einsparen?
Nein, denn es sind immer noch drei Kanäle mit z.B. 8 Bit Auflösung pro Kanal
#### b) Kann ein Beamer ein Bikld im YCbCr-Farbbereich darstellen?
Nein, der Beamer verwendet drei Farbkanäle/Farbkanonen im additiven Farbsystem. Darum wird RGB benötigt.
#### c) Wie rechnet man ein Farbbildes in ein Graustufenbild um?
Graustufenbild Farbanteile: Grün 60%, Rot 30% und Blau 10%
#### d) Warum hat bei der Umwandlung eine Farbbildes in ein Graustufenbild der Grünanteil am meisten Gewicht?
Das menschliche Auge ist für Grün am meisten empfindlich. Das hat einen evolutionsbiologischen Hintergrund. Danach folgt Rot und für Blau ist man am wenigsten empfindlich, mal von den Schlümpfen abgesehen

### 5.) Der folgende Youtube-Film beschäftigt sich mit Chroma-Subsampling: https://www.youtube.com/watch?v=Nd-7o3o5x6A Schauen sie den an und beantworten sie anschliessend diese Fragen:
#### a) Warum verschlechtert sich die Bildschärfe von 4:1:1-Subsampling gegenüber 4:4:4-Subsampling nicht?
Der Luminanzkanal (Graustufenkanal) hat bei beiden Varianten eine unverändert hohe Auflösung. Dies ist entscheidend für die Bildschärfe
#### b) Ein quadratisches 24-Bit-RGB-Bild mit einer Kantenlänge von 1000 Pixel soll mit 4:1:1 unterabgetatstet werden. Wieviel Speicherplatz wird damit eingespart?
Original: 100% + 100% + 100% = 300% Dem gegenüber bedeutet 4:1:1 folgendes: 100% + 25% + 25% = 150%
Somit belegt 4:1:1 die Hälfte des Speicherplatzes (150%) vom Original (300%)


### 6.) Der folgende Youtube-Film beschäftigt sich mit der JPG-Komprimierung: https://www.youtube.com/watch?v=Kv1Hiv3ox8I Schauen sie den an und beantworten sie anschliessend diese Fragen:
#### a) Was ist der erste Schritt bei der JPG-Komprimierung?
Umwandlung von RGB in YCbCr, Danach Subsampling
#### b) Führt die DCT-Transformation zu einer Datenreduktion?
Nein, es sind pro Block immer noch 8*8=64 Werte. Erst die anschliessende Quantisierung und der dadurch effiziente RLC bringt eine Datenreduktion
#### c) Warum erhält man bei einer sehr starken Bildkomprimierung sogenannte Block-Artefakte?
Wegen der Unterteilung in 8*8-Pixel Blöcke, die sich - vereinfacht ausgedrückt - in der Farbe immer mehr angleichen und als Quadrate im Bild wahrgenommen werden

### 7.)Der folgende Youtube-Film beschäftigt sich mit Codecs und Containern: https://www.youtube.com/watch?v=-4NXxY4maYc Schauen sie den an und beantworten sie anschliessend diese Fragen:
#### a) Was ist der Unterschied zwischen Intraframe- und Interframe-Komprimierung?
Intraframe: Komprimierung innerhalb eines Bildes
#### b) Bei welcher Filmsequenz bietet die Interframekomprimierung mehr Potential zur Datenreduzierung:
##### i.) 30 Sekunden-Szene mit Faultier auf Nahrungssuche?
Hat viel Potential bei der Interframekomprimierung, weil die Bilder sich kaum unterscheiden.
##### ii.) 30 Sekunden-Szene mit Zieleinfahrt beim Formel-1-Rennen?
Hat wenig Potenzial, weil sich die Bilder sehr unterscheiden
#### c) Sehen sie Parallelen zwischen Datenbackupkonzepten und Interframe-Komprimierung?
Ja, auch beim Datenbackup muss immer mal wieder ein Fullbackup (vgl. I-Frame) erstellt werden.
#### d) Was versteht man unter GOP25?
Jedes 25. Bild ist ein komplettes Bild. (I-Frame oder Schlüsselbild)
