# Aufgaben zu numerischen Codes

## 1.)

![tabelle_ausfüllen](./Images/tabelle_ausfüllen.png)

## 2, 3, 4)

| Dezimal | Binär               | Hexadezimal |
| ------- | ------------------- | ----------- |
| 911     | 0011 1000 1111      |             |
| 182     | 1011 0110           |             |
|         | 1110 0010 1010 0101 | E2A5        |

## 5.)

1101 1001 (217) + 0111 0101 (117) = 1010 1100 (172)

## 6.

### a)

1100’0000.1010’1000.0100’1100.1101’0011 = 192.168.76.211
Bedeutung: IP-Adresse

### b)

1011’1110-1000’0011-1000’0101-1101’0101-1110’0100-1111’1110 = BE-83-85-D5-E4-FE
Bedeutung: MAC-Adresse

## 7.) _Optional_

## 8.)

2^7

## 9.) _Optional_

## 10.) _Optional_

## 11.

### a)

- Der kleinste Binärwert ist 0000 0000, was im Dezimalsystem 0 entspricht.
- Der größte Binärwert ist 1111 1111, was im Dezimalsystem 255 entspricht.

### b)

- Der kleinste Binärwert ist 1000 0000, was im Dezimalsystem -128 entspricht.
- Der größte Binärwert ist 0111 1111, was im Dezimalsystem 127 entspricht.

### c, d)

0 = positiv
1 = negativ

| Dezimal | Binär       |
| ------- | ----------- |
| +83     | 0 0101 0011 |
| -83     | 1 0101 0011 |

### e)

0 0101 0011 + 1 0101 0011 = 0000 0000

### f)

| Dezimal | Binär     |
| ------- | --------- |
| 0       | 0000 0000 |

Ja, meine vorherige Addition hat auch diesen Binärwert gegeben.

### g)

Die Zahl +150 überschreitet den maximal möglichen positiven Wert (+127) in einem 8-Bit vorzeichenbehafteten System. Deshalb kann +150 nicht korrekt in einem Byte dargestellt werden, wenn es als vorzeichenbehaftete Ganzzahl interpretiert wird.

## 12.)

Manchmal werden Bruch- oder Dezimalwerte als ganze Zahlen dargestellt. Für solche Fälle sind, sind Fliesskommazahlen die richtige Wahl.
Normalerweise würde ein int von 0.3333333 so aussehen: 0, aber dank float sieht es als ihre aktuelle Form.

## 13.) _Optional_

## 14.) _Optional_

# Aufgaben zu alphanumerischen Codes

## Welche der Dateien ist nun ASCII-codiert, welche UTF-8 und welche UTF-16 BE-BOM?

- Textsample1: ASCII
- Textsample2: UTF-8
- Textsample3: UTF-16 BE-BOM

## Alle drei Dateien enthalten denselben Text. Aus wie vielen Zeichen besteht dieser?

- 68 Zeichen

## Dateigrössen?

- Textsample1: 68 Bytes
- Textsample2: 71 Bytes
- Textsample3: 138 Bytes

## ASCII- und die UTF-8-Datei

- Umlaut: ä
- Eurozeichen: €

## Big-Endian (BE), Little-Endian (LE) in UTF-16

- Big-Endian: Byte-Reihenfolge wie hh:mm:ss
- Little-Endian: Byte-Reihenfolge wie dd:mm:yyyy

## Änderungen?

- Durch das Abwechseln der Encoding von ASCII zu UTF abwechselt, werden die Zeichen der Texten angepasst
