# Aufgaben zu Daten Komprimierung

## 1.) Huffman-Algorithmus

### Wo Baumstrukturen zur Anwendung kommen?

- Bei Hierarchische Dateisysteme können Baumstrukturen vorkommen.

### Was unterscheidet einen binären Baum von einem nicht binären Baum?

- Ein binärer Baum unterscheidet sich von einem nicht binären Baum dadurch, dass jeder Knoten maximal zwei Kinder haben kann. Bei nicht binären Bäumen kann ein Knoten beliebig viele Kinder haben.

## 2.) Huffman-Algorithmus

![Hoffmann_Task](./Images/Hoffmann_Task.jpg)

## 3.) RLC

### Welche Bitbreite (1011Grün ergäbe 4 Bit) wäre bei einem quadratischen Bild mit 20 Pixel Kantenlänge sinnvoll? Was wäre, wenn dieses Bild nur aus einer Farbe besteht?

- Für ein 20x20 Pixel Farbfoto sind 8 Bit pro Farbkanal (24 Bit insgesamt) sinnvoll. Für ein Schwarz-Weiss-Bild reichen 1 Bit pro Pixel aus.

## 4.) RLC

![rls_task](./Images/rlc_task.jpg)

### Was stellt sie dar?

Eine Figur

## 5.) LZW Verfahren

### Erstellen sie die LZW-Codierung für das Wort «ANANAS» und überprüfen sie mit der Dekodierung ihr Resultat.

| Schritt | Zeichenkette | Gefunden    | Gespeichert | Temp. Wörterbucheintrag |
| ------- | ------------ | ----------- | ----------- | ----------------------- |
| 1.      | *A*NANAS     | A           | A           | AN -> "256"             |
| 2.      | A*N*ANAS     | N           | N           | NA -> "257"             |
| 3.      | AN*A*NAS     | AN -> "256" | "256"       | ANA -> "258"            |
| 4.      | ANA*N*AS     | NA -> "257" | "257"       | NAS -> "259"            |
| 5.      | ANAN*A*S     | A           | A           | AS -> "260"             |

### Versuchen sie den erhaltenen LZW-Code «ERDBE<256>KL<260>» zu dekomprimieren

| Schritt | Übermitteltes Zeichenkette | Aktuelles Zeichen | Ausgae | Temp. Wörterbucheintrag |
| ------- | -------------------------- | ----------------- | ------ | ----------------------- |
| 1.      | *E*RDBE<256>KL<260>        | E                 | E      | -                       |
| 2.      | E*R*DBE<256>KL<260>        | R                 | R      | ER -> "256"             |
| 3.      | ER*D*BE<256>KL<260>        | D                 | D      | RD -> "257"             |
| 4.      | ERD*B*E<256>KL<260>        | B                 | B      | DB -> "258"             |
| 5.      | ERDB*E*<256>KL<260>        | E                 | E      | BE -> "259"             |
| 6.      | ERDBE*<256>*KL<260>        | E                 | ER     | EE -> "260"             |
| 7.      | ERDBE<256>*K*L<260>        | K                 | K      | RK -> "261"             |
| 8.      | ERDBE<256>K*L*<260>        | L                 | L      | KL -> "262"             |
| 9.      | ERDBE<256>KL*<260>*        | E                 | EE     | EL -> "263"             |

- Ausgabe: ERDBEERKLEE

## 6.) _Optional: BWT (Burrows-Wheeler-Transformation)_

## 7.) _Optional: ZIP-Komprimierung:_

## 8.) Weitere verlustlose Verfahren

### Kenne sie noch weitere Verfahren, wo verlustlos komprimiert wird?

- Prediction by Partial Matching (PPM)

### Welche Daten sollen überhaupt verlustlos komprimiert werden?

Verlustlose Komprimierung wird für Daten verwendet, bei denen es wichtig ist, dass die Originaldaten nach der Dekomprimierung vollständig und exakt wiederhergestellt werden können.
Beispiele dazu:

- Textdateien
- Backups und Archivierungen

### Was würde passieren, wenn man ein Brief oder ein Java-Sourcecode verlustbehaftet komprimieren würde?

Verlustbehaftete Kompression von Briefen oder Java-Code führt zu Verständlichkeits- und Syntaxfehlern, Bedeutungsverlust, Sicherheitsrisiken, und zerstört Datenintegrität und Verlässlichkeit.
